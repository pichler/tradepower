
<!-- README.md is generated from README.Rmd. Please edit that file -->

# tradepower <a href='http://www.pik-potsdam.de/~pichler/metab/blog/'><img src='man/figures/logo.png' align="right" height="138.5" /></a>

<!-- badges: start -->

[![License: GPL
v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![CRAN\_Status\_Badge](http://www.r-pkg.org/badges/version/tradepower)](https://cran.r-project.org/package=tradepower)
<!-- badges: end -->

## Installation

Install the most up-to-date development version from Paul’s gitlab using
your personal gitlab credentials:

``` r
cred <- git2r::cred_user_pass(rstudioapi::askForPassword("username"), rstudioapi::askForPassword("Password"))
remotes::install_git("https://gitlab.pik-potsdam.de/pichler/tradepower", credentials = cred)
```

## Functions available

| Function          | Description                                                   |
|-------------------|---------------------------------------------------------------|
| `read_BACI`       | Read trade data from BACI, clean up and reformat              |
| `edge_asymmetry`  | Calculate edge asymmetry                                      |
| `summarize_edges` | Summarize edges (across possible products or duplicate flows) |
| `summarize_nodes` | Summarize nodes (across trade flows)                          |

## Input data frame from BACI

The `read_BACI` function requires `.csv` files with the format:

| t                | k                     | i                   | j                   | v                   | q                     |
|------------------|-----------------------|---------------------|---------------------|---------------------|-----------------------|
| `year` e.g. 2019 | `product` e.g. 300110 | `exporter` e.g. DEU | `importer` e.g. FRA | `value` e.g. 123.45 | `quantity` e.g. 67.89 |
| …                | …                     | …                   | …                   | …                   | …                     |

## License

Distributed under the GNU Public License. See `LICENSE.md` for more
information

## Acknowledgements

Trade Data available from
[CEPII](http://www.cepii.fr/CEPII/en/bdd_modele/download.asp?id=37).

<div>

Icon made by
<a href="https://www.flaticon.com/authors/photo3idea-studio" title="photo3idea_studio">photo3idea\_studio</a>
from
<a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>

</div>
