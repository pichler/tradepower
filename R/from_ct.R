#' Import trade data from comtradr package
#'
#' @param .data
#' Data.frame with trade flow data
#'
#' @return
#' Data.frame formatted for summarize_edges()
#'
#' @export

from_ct <- function(.data) {
  .data %>%
    {if(unique(.data$trade_flow_code)==1) dplyr::rename(.data,"importer"="reporter_iso", "exporter"="partner_iso", "value"="trade_value_usd", "quantity"="netweight_kg","product"="commodity_code")
        else if(unique(.data$trade_flow_code)==2) dplyr::rename(.data,"importer"="reporter_iso", "exporter"="partner_iso", "value"="trade_value_usd", "quantity"="netweight_kg","product"="commodity_code")
                else .data} %>%
    dplyr::select(.data$year,.data$importer,.data$exporter,.data$product,.data$value,.data$quantity)
}
