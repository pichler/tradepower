test_that("edge list matches expected result", {
  testdata <- read.csv(system.file("testdata", "edges_result.csv", package="tradepower"))
  outdata <- read.csv(system.file("testdata", "asymmetry_result.csv", package="tradepower"))
  expect_equal(as.data.frame(edge_asymmetry(testdata, year, exporter, importer)), outdata)
})
